#pragma once

#include <iostream>

namespace tools {

	template < typename TCollection >
	void foreach_print(TCollection const & _collection, std::ostream & _o = std::cout)
	{
		for (auto const & el : _collection)
			_o << el << std::endl;
	}
}